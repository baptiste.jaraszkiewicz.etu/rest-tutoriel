package fr.ulille.iut.tva.dto;

import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TauxDto {
	private String label;
	private double taux,montantTotal, montantTva, somme;
	
	public TauxDto() {}
	
	public TauxDto(InfoTauxDto infoDto, double montantTotal, double montantTva, double somme) {
		super();
		this.label = infoDto.getLabel();
		this.taux = infoDto.getTaux();
		this.montantTotal = montantTotal;
		this.montantTva = montantTva;
		this.somme = somme;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public double getTaux() {
		return taux;
	}

	public void setTaux(double taux) {
		this.taux = taux;
	}

	public double getMontantTotal() {
		return montantTotal;
	}
	
	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}
	public double getMontantTva() {
		return montantTva;
	}
	public void setMontantTva(double montantTva) {
		this.montantTva = montantTva;
	}
	public double getSomme() {
		return somme;
	}
	public void setSomme(double somme) {
		this.somme = somme;
	}
}
