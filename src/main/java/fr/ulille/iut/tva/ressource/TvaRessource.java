package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.dto.TauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
	@SuppressWarnings("unused")
	private CalculTva calculTva = new CalculTva();

	@GET
	@Path("tauxpardefaut")
	public double getValeurTauxParDefaut() {
		return TauxTva.NORMAL.taux;
	}

	@GET
	@Path("{niveauTva}")
	public double getMontantTotal(@PathParam("niveauTva") String niveau, @QueryParam("somme") int somme) {
		//return (1 + TauxTva.valueOf(niveau.toUpperCase()).taux / 100) * somme;
		try {
			return new CalculTva().calculerMontant(TauxTva.valueOf(niveau.toUpperCase()),(double) somme);
		}
		catch ( Exception ex ) {
			throw new NiveauTvaInexistantException();
		}
	}

	@GET
	@Path("valeur/{niveauTva}")
	public float getValeurTaux(@PathParam("niveauTva") String niveau) {
		try {
			return (float) TauxTva.valueOf(niveau.toUpperCase()).taux; 
		}
		catch ( Exception ex ) {
			throw new NiveauTvaInexistantException();
		}
	}

	@GET
	@Path("lestaux")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public List<InfoTauxDto> getInfoTaux() {
		ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
		for ( TauxTva t : TauxTva.values() ) {
			result.add(new InfoTauxDto(t.name(), t.taux));
		}
		return result;
	}

	@GET
	@Path("details/{niveauTva}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public TauxDto getDetail(@PathParam("niveauTva") String niveau, @QueryParam("somme") double somme) {
		TauxTva t = TauxTva.valueOf(niveau.toUpperCase());
		InfoTauxDto infoTaux = new InfoTauxDto(t.name(), t.taux);

		double montant = new CalculTva().calculerMontant(TauxTva.valueOf(niveau.toUpperCase()),somme);
		
		return new TauxDto(infoTaux, montant, montant - somme, somme);
	}


}
